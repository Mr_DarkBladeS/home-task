import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

class TestSort {	
	@Test
	public void test() {
		int[] sort = {15, 20, 7, 3, 10};
		int[] niceSort = {3, 7, 10, 15, 20};
		
		Sort.bubbleSort(sort);

		assertTrue("Arrays are not equal", java.util.Arrays.equals(sort, niceSort));
	}

}